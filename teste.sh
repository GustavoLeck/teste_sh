#!/bin/bash

#Conectando ao usuario root
sudo su

#Instalando as ferramentas 
apt-get update
apt-get install vim net-tools nmap aptitude -y
aptitude install gnupg

#Instalando Owncloud
wget -nv https://download.owncloud.org/download/repositories/production/Debian_10/Release.key -O - | apt-key add -
echo 'deb https://download.owncloud.org/download/repositories/production/Debian_10/ / ' | tee -add
/etc/apt/sources.list.d/owncloud.list
aptitude update
aptitude install owncloud-complete-files

#Instalando server e banco de dados
aptitude install apache2 mariadb-server mariadb-client -y
wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
echo "deb https://packages.sury.org/php/ $(lsb_realese -sc) main" > /etc/apt/sources.list.d/php.list

#Instalando PHP 7.2
aptitude update
aptitude install php7.2 libapache2-mod-php7.2-{mysql,intl,curl,json,gd,xml,mbstring,zip} -y

#Criando banco de dados e configurando acesso root
mysql -u root -p 
CREATE DATABASE owncloud;
GRANT ALL ON owncloud.* TO 'owncloud_user'@'localhost' IDENTIFIED BY 'StrongP@ssword';
FLUSH PRIVILEGES;
EXIT;

#Configura acesso pelo navegador



#Finalizando config
ln -s /etc/apache2/sites-0avaible/owncloud.conf  /etc/apache2/sites-enabled
a2enmod rewrite mime unique_id
systemctl restart apache2




echo "--Instalação e configuração feita com exito!--
